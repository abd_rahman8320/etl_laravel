<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Log;
use App\Jobs\BaselineMongoJobs;

class SynchronizationController extends Controller
{
    public function baseline_to_mongodb()
    {
        BaselineMongoJobs::dispatch();
    }
}
