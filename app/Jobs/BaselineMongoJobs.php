<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use DB;
use Log;

class BaselineMongoJobs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '10240M');

        $date = date('Y-m-d H:i:s');

        DB::connection('mongodb')
        ->collection('master_baseline_onair_4g_monthly')
        ->truncate();

        try {
            Log::info('get data');

            $data = DB::connection('mysql2')
            ->select('select * from master_baseline_onair_4g_monthly');

        } catch (\Exception $e) {
            dd($e->getMessage());
        }

        Log::info('insert');

        foreach ($data as $row) {
            $row = json_decode(json_encode($row), true);
            $row['input_date_mongodb'] = $date;

            DB::connection('mongodb')
            ->collection('master_baseline_onair_4g_monthly')
            ->insert([$row]);
        }

        return 'Success';
    }
}
